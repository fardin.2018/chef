powershell_script 'Install IIS' do 
  code 'Install-windowsFeature Web-Server'
  not_if "(Get-WindowsFeature -Name Web-server).installed"
end

service 'w3svc' do 
  action [:start, :enable]
end